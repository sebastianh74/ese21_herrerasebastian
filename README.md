# ese21_HerreraSebastian

# Sistema de control para espectrofotómetro de luz ultravioleta y visible

## Descripción General

El sistema de software de control forma parte de un proyecto de actualización del hardware y software de un Espectrofotómetro tipo doble haz de luz ultravioleta y visible. Actualmente el instrumento se encuentra en deshuso ya que el sistema electronico de medicion y visualizacion de los datos se encuentra dañado. 
La idea del Instituo es utilizar el sistema optico del instrumento, que se encuentra en perfecto estado,  y adptarle un sistema embebido.


### Funciones del Producto
El software aquí especificado brindará las siguientes funcionalidades.
* Control básico sobre el funcionamiento del espectrofotómetro. 
* Control del servomotor para seleccionar lámpara del ensayo (Puede ser ultravioleta o visible).
* Control de motor paso a paso unipolar mediante señal de reloj, habilitacion/deshabilitacion y cambio de  sentido de giro, a los fines de poder variar la longitud de onda a travez de un sistema de tornillo micrometrico y rejilla de difraccion.
* Lectura y guardado de las señales analogicas provenientes del fotodetector.
* Comunicación mediante UART del microcontrolador con un computadora PC de escritorio, a los fines de procesar las mediciones.

El software aquí especificado no brindará los servicios de
* Autocalibración.
* Control de tensión en fuente de alimentación.

### Características de los Usuarios
1. Los usuarios finales de este producto son becarios de un laboratorio de Química con amplia experiencia en el uso de espectrofotómetro (INQUINOA-CONICET).
2. Adicionalmente serán usuarios de este producto, técnicos electrónicos quienes se ocupan del mantenimiento del equipo.

### Restricciones
* El software debe mantenerse bajo control de versiones
* El software de microcontrolador deberá programarse en lenguaje C

## Diagrama de bloques del sistema propuesto

Poner diagrama de bloques 

![Diagram de bloques.](/img/bloquehard.png "This is a sample image.")









